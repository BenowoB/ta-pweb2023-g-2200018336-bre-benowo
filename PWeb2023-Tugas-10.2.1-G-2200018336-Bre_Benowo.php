<?php

	echo "Tugas 10.2.1 (konversi nilai angka ke huruf)<br><br>";

	

function konversiNilai($nilai) {
    if ($nilai >= 80 && $nilai <= 100) {
        return 'A';
    } elseif ($nilai >= 65 && $nilai < 80) {
        return 'B';
    } elseif ($nilai >= 50 && $nilai < 65) {
        return 'C';
    } elseif ($nilai >= 25 && $nilai < 50) {
        return 'D';
    } elseif ($nilai >= 0 && $nilai < 25) {
        return 'E';
    } else {
        return 'Nilai tidak valid';
    }
}
	echo "<br><br>";
$nilai = 99;
$nilaiHuruf = konversiNilai($nilai);

	if ($nilai >= 60) {
		echo "Nilai Anda $nilai, Anda Lulus";
	} else {
		echo "Nilai Anda $nilai, Anda Gagal";
	}
	echo "<br><br> ";
	echo "Nilai {$nilai} setara dengan {$nilaiHuruf} <br><br>";
	echo "<br><br>";
$nilai = 73;
$nilaiHuruf = konversiNilai($nilai);

	if ($nilai >= 60) {
		echo "Nilai Anda $nilai, Anda Lulus";
	} else {
		echo "Nilai Anda $nilai, Anda Gagal";
	}
	echo "<br><br> ";
	echo "Nilai {$nilai} setara dengan {$nilaiHuruf} <br><br>";
	echo "<br><br>";
	$nilai = 62;
$nilaiHuruf = konversiNilai($nilai);

	if ($nilai >= 60) {
		echo "Nilai Anda $nilai, Anda Lulus";
	} else {
		echo "Nilai Anda $nilai, Anda Gagal";
	}
	echo "<br><br> ";
	echo "Nilai {$nilai} setara dengan {$nilaiHuruf} <br><br>";
	echo "<br><br>";
	$nilai = 47;
$nilaiHuruf = konversiNilai($nilai);

	if ($nilai >= 60) {
		echo "Nilai Anda $nilai, Anda Lulus";
	} else {
		echo "Nilai Anda $nilai, Anda Gagal";
	}
	echo "<br><br> ";
	echo "Nilai {$nilai} setara dengan {$nilaiHuruf} <br><br>";
	echo "<br><br>";
	$nilai = 17;
$nilaiHuruf = konversiNilai($nilai);

	if ($nilai >= 60) {
		echo "Nilai Anda $nilai, Anda Lulus";
	} else {
		echo "Nilai Anda $nilai, Anda Gagal";
	}
	echo "<br><br> ";
	echo "Nilai {$nilai} setara dengan {$nilaiHuruf} <br><br>";

?>