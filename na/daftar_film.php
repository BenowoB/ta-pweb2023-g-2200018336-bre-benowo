<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>TIKET BIOSKOP - Daftar Film</title>
</head>

<body>
    <header>
        <h1>TIKET BIOSKOP</h1>
    </header>
    <nav>
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="daftar_film.php">Daftar Film</a></li>
            <li><a href="booking.php">Booking Tiket</a></li>
            <li><a href="ulasan.html">Ulasan</a></li>
            <li><a href="ada.html">Bebas Kirim</a></li>
        </ul>
    </nav>
    <main>
        <h2>Daftar Film di Bioskop Bre Benowo</h2>
        <div class="container">
            <div class="card-container">
                <div class="card">
                    <img src="film1.jpeg" alt="Film Si Kecil">
                    <h2>Film Si Kecil</h2>
                    <p>Harga: Rp 50.000</p>
                </div>
                <div class="card">
                    <img src="film2.jpeg" alt="Film Si Kelinci">
                    <h2>Film Si Kelinci</h2>
                    <p>Harga: Rp 50.000</p>
                </div>
                <div class="card">
                    <img src="film3.jpeg" alt="Film Si Raksasa">
                    <h2>Film Si Raksasa</h2>
                    <p>Harga: Rp 50.000</p>
                </div>
            </div>
        </div>
    </main>
    <footer>
        <p>&copy;
            <?php echo date("Y"); ?> Bioskop Bre Benowo. All rights reserved.
        </p>
    </footer>
    <script src="script.js"></script>
</body>

</html>