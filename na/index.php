<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>TIKET BIOSKOP</title>
</head>

<body>
    <header>
        <h1>TIKET BIOSKOP</h1>
    </header>
    <nav>
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="daftar_film.php">Daftar Film</a></li>
            <li><a href="booking.php">Booking Tiket</a></li>
            <li><a href="ulasan.html">Ulasan</a></li>
            <li><a href="ada.html">Bebas Kirim</a></li>
        </ul>
    </nav>
    <main>
        <h2>Selamat datang di Bioskop Bre Benowo</h2>
        <p>Sekarang Sedang Promo Untuk pemesanan </p>
        <img src="th.jpeg">
    </main>
    <footer>
        <p>&copy;
            <?php echo date("Y"); ?> Bioskop Bre Benowo. All rights reserved.
        </p>
    </footer>
    <script src="script.js"></script>
</body>

</html>